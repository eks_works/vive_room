﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PicturesController : MonoBehaviour {

    [Serializable]
    public class ListOfPictures
    {
        public List<Pictures> list;
    }

    [Serializable]
    public class Pictures
    {
        public string TexturePath;// name
        public string Header;// header
        public string Description;//
    }

    public string ListFile = "";
    
    [SerializeField]
    public ListOfPictures List;
    public PictureTexture[] pictures;


    IEnumerator Loading()
    {
        string name = "file:///" + Application.dataPath + "/../" + ListFile;
        // Debug.Log("[PATH]:" + name);

        WWW www = new WWW(name);

        yield return www;

        if (www.isDone)
        {
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogWarning(www.error);
            }
            else
            {
                Debug.Log("[RAW]" + www.text);

                List = JsonUtility.FromJson<ListOfPictures>(www.text);

                if(List != null && List.list != null)
                {
                    var min = Mathf.Min(pictures.Length, List.list.Count);

                    for (int i = 0; i < min; i++)
                    {
                        pictures[i].UpdatePictures(List.list[i]);
                    }
                }
            }
        }
    }

    void Start () {

        pictures = transform.GetComponentsInChildren<PictureTexture>();

       // string s = JsonUtility.ToJson(List, true);
        //File.WriteAllText(ListFile, s);

     //   Debug.Log("List:\n" + s);
        StartCoroutine(Loading());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
