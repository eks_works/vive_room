﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampBreakController : MonoBehaviour {

    public float TriggerDalayTime = 1f;

    private Animator _anim;
    private float Last = 0;

    private AudioSource _source;
	void Start () {
        _anim = GetComponent<Animator>();
        _source = GetComponent<AudioSource>();
	}
    
    public void PlayEffect()
    {
        _source.Play();
    }

    public void OnRestore()
    {
        _anim.SetTrigger("Restore");
    }
    public void OnTriggered(bool force = false)
    {
        if (force)
        {

            _anim.SetTrigger("ForcedBreak");
            return;
        }
        Debug.Log("Trigged");
        if(Last == 0  || Time.timeSinceLevelLoad - Last >= TriggerDalayTime)
        {
            _anim.SetTrigger("Trigged");
            Last = Time.timeSinceLevelLoad;
        }
    }
}
