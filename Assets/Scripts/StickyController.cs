﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyController : MonoBehaviour {

    public string StickPlane;
    public float DropSpeedFactor = 0.5f;
    Rigidbody _rigid;

    BossLogicController _blc;

    private void Start()
    {
        _blc = BossLogicController.Instance;
        _rigid = GetComponent<Rigidbody>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == StickPlane)
        {
            _rigid.isKinematic = true;

            if(_blc != null)
            {
                _blc.OnShootToPicture(other.transform.parent.name);
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!_rigid)
            return;
        _rigid.velocity *= DropSpeedFactor;
        var lbc = collision.collider.GetComponentInParent<LampBreakController>();
        if (lbc != null)
            lbc.OnTriggered();

    }
}
