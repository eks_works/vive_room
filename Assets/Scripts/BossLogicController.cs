﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class BossLogicController : MonoBehaviour {

    public static BossLogicController Instance;

    public Transform[] pictures;
    public LampBreakController[] LinkedLamps;
    public LampBreakController[] AllLamps;

    public GameObject MarshBoss;
    public LampBreakController MarshBossLamp;
   // public float DelayTime = 3f;
    public AudioSource[] Speakes;
    public AudioClip HororAmbiend;
    public AudioClip Switch;

    private List<string> TIds = new List<string>();
    private int index = 0;
    private AudioSource _source;

	void Start () {

        if (Instance != this)
        {
            if (Instance != null)
                Destroy(Instance);
            Instance = this;
        }

        foreach (var item in pictures)
        {
            TIds.Add(item.gameObject.name);
        }

        System.Random rnd = new System.Random();
        //TIds = TIds.OrderBy(x => rnd.Next()).ToList();
        _source = GetComponent<AudioSource>();
    }
    public void DelayedShow()
    {
        _source.PlayOneShot(Switch);
        MarshBoss.SetActive(true);
        MarshBossLamp.OnRestore();
    }

    public void OnShootToPicture(string targetName)
    {
        if (index < LinkedLamps.Length)
        {
           // if (TIds[index] == targetName)
           // {
                LinkedLamps[index].OnTriggered(true);
                index++;

                if (index == LinkedLamps.Length)
                {
                    foreach (var item in AllLamps)
                    {
                        item.OnTriggered(true);
                    }

                    foreach (var item in Speakes)
                    {
                        item.Stop();
                    }

                    _source.PlayOneShot(HororAmbiend);
                    Invoke("DelayedShow", HororAmbiend.length);
                }

        /*}
            else
            {
                index = 0;

                foreach (var item in LinkedLamps)
                {
                    item.OnRestore();
                }
            }
            */
        }
    }


	void Update () {
		
	}
}
