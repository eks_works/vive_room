﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ShotController : MonoBehaviour {

    public Transform Bullet;
    public float FireRate = 0;
    public float FireForce = 100;
    public int MagazinSize = 10;
    public Transform FirePoint;

    [Space(10)]

    public AudioClip Shot;
    public AudioClip PickUp;

    [Space(10)]
    public ParticleSystem[] particles;
    public int EmitCount = 10;

    //private bool CanFire = false;

    private Hand _hand;
    private int bulletsCount = 0;
    private Throwable _throwable;
    private AudioSource _source;
    float _lastRateTime = 0;

    void Start() {

        bulletsCount = MagazinSize;
        if (FirePoint == null)
            FirePoint = transform.FindChild("FirePoint");

        _throwable = GetComponent<Throwable>();
        _throwable.onDetachFromHand += OnDetach;
        _throwable.onPickUp += OnAttach;

        _source = GetComponent<AudioSource>();
    }

    // Update is called once per
    void Update() {

        if (_hand != null && _hand.controller != null)
        {
            if (_hand.controller.GetHairTriggerDown() && Time.timeSinceLevelLoad - _lastRateTime >= FireRate * 0.001f)
            {
                Fire();
                _lastRateTime = Time.timeSinceLevelLoad;
            }
        }
    }

    void Fire()
    {
        if (FirePoint == null || Bullet == null)
            return;

        if (bulletsCount > 0)
        {
            var obj = Instantiate(Bullet, FirePoint.position, FirePoint.rotation) as Transform;

            var rg = obj.GetComponent<Rigidbody>();
            rg.AddForce(obj.forward * FireForce * rg.mass, ForceMode.Impulse);
            
            EmitParticles();

            if(Shot != null)
                _source.PlayOneShot(Shot);
            //bulletsCount--;
        }
        
    }
    

    void EmitParticles()
    {
        foreach (var item in particles)
        {
            item.Emit(EmitCount);
        }
    }
    void OnAttach(Hand hand)
    {
        _hand = hand;
    //    CanFire = true;
        _lastRateTime = Time.timeSinceLevelLoad - FireRate * 0.001f;

        Debug.Log("[" + name + " Atached]");

        if(PickUp != null)
            _source.PlayOneShot(PickUp);
    }
    void OnDetach()
    {
        _hand = null;
     //   CanFire = false;

        Debug.Log("[" + name + " detached]");


        if (PickUp != null)
            _source.PlayOneShot(PickUp);
    }
}
