﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManController : MonoBehaviour {

    public Transform Target;
    public float Distance=2f;
    private Animator _anim;
    private bool talk = false;

    void Start () {
        _anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        var d = Vector3.Distance(transform.position, Target.position);
        if(!talk && d < Distance)
        {
            _anim.SetTrigger("Near");
            talk = true;
        }
	}
}
