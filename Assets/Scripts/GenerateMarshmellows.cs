﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMarshmellows : MonoBehaviour {

    public enum SpawnType { Ones, OnesWithInterval };
    public GameObject Prefab;
    public Vector3 RandomOffsets = new Vector3(0.1f, 0.1f, 0.1f);
    public float SpawnInterval = 0.1f;
    public int Count = 25;
    public SpawnType Type = SpawnType.OnesWithInterval;

	void Start () {

        StartCoroutine(Spawner());
	}
	
    IEnumerator Spawner()
    {


        for(int i=0; i <  Count; i++)
        {
            var pos = transform.position;
            pos.x += Random.Range(-RandomOffsets.x, RandomOffsets.x);
            pos.y += Random.Range(-RandomOffsets.y, RandomOffsets.y);
            pos.z += Random.Range(-RandomOffsets.z, RandomOffsets.z);

            Instantiate(Prefab, pos, Quaternion.identity, transform);
            if(Type == SpawnType.OnesWithInterval)
            {
                yield return new WaitForSeconds(SpawnInterval);
            }
        }

        yield return new WaitForEndOfFrame();
    }
	// Update is called once per frame
	void Update () {
		
	}
}
