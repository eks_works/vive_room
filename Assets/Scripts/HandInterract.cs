﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;

public class HandInterract : MonoBehaviour {

    public Animator TargetHandAnimator;
   
    private Hand _hand;

    void Start () {
        _hand = GetComponent<Hand>();
	}
	
	void Update () {

        if (_hand.controller == null)
            return;

        bool _grip = _hand.IsAttachedOther || _hand.controller.GetPress(EVRButtonId.k_EButton_Grip);
        TargetHandAnimator.SetBool("isGrip", _grip);
        
      
	}
    
}
