﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PictureTexture : MonoBehaviour {

    public string FileName;
    private Texture2D _loadedtexture;
    public int TargetMaterialIndex = 1;
    public Text header;
    public Text Description;

    public PicturesController.Pictures data;

    private Material _target;

    IEnumerator Loading(PicturesController.Pictures pic)
    {
       // data = pic;
        string name = "file:///"+Application.dataPath + "/../" + pic.TexturePath;
       // Debug.Log("[PATH]:" + name);

        WWW www = new WWW(name);

        yield return www;

        if(www.isDone)
        {
            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogWarning(www.error);
            }
            else
            {
                _loadedtexture = www.texture;
                LoadingEnd(_loadedtexture);
            }
        }
    }
	
    public void LoadingEnd(Texture2D _tex)
    {
        if (_target != null)
            _target.mainTexture = _tex;

        if(header != null)
            header.text = data.Header;
        if (Description != null)
            Description.text = data.Description;
    }
    
	void Start () {

        var _M = GetComponent<Renderer>().materials;
        if (TargetMaterialIndex < _M.Length)
            _target = _M[TargetMaterialIndex];
        //StartCoroutine(Loading());
	}

    public void UpdatePictures(PicturesController.Pictures pic)
    {
        data = pic;
        StartCoroutine(Loading(pic));
    }
    
	
}
