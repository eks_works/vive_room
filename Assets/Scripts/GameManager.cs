﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject PressAgainHint;
    public float HideTime = 3f;

    private bool showed = false;
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {

        if (Player.instance == null)
            return;
        foreach (var item in Player.instance.hands)
        {
            if(item.controller != null)
                if (item.controller.GetPressDown(EVRButtonId.k_EButton_ApplicationMenu))
                    Show();
        }
	}

    public void Show()
    {

        if (!showed)
        {
            PressAgainHint.SetActive(true);
            Invoke("Hide", HideTime);
        }
        else
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        showed = true;



    }

    public void Hide()
    {
        PressAgainHint.SetActive(false);
        showed = false;
    }
}
